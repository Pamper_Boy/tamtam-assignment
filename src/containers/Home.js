import React, { PureComponent, Fragment } from 'react'

import { ImageSlider, Section, InstaFeed } from '../components'
import { instagramItems } from '../components/data.js'


class Home extends PureComponent {


  render() {
    return (
      <Fragment>
        <ImageSlider/>
        <Section
          title="We are TamTam"
          text="Tam Tam is a full service digital agency focusing on Dutch Digital Service Design. We combine strategy, design, technology and interaction to make the digital interactions between company and customer valuable and memorable. We work for awesome brands with a team of 120 digitals from our office in Amsterdam. Making great work and having a blast doing it. That’s what we believe in."
        />
        <InstaFeed title="FOLLOW US ON INSTAGRAM" subtitle="@tamtamnl" items={instagramItems}/>
      </Fragment>
    )
  }
}

export default Home
