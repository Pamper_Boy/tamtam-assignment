import React, { Component } from 'react';
import Navbar from './components/ui/Navbar'
import Footer from './components/ui/Footer'
import Routes from './routes'


class App extends Component {
  render() {
    return (
      <div className="App">
        <Navbar/>
        <Routes />
        <Footer />
      </div>
    );
  }
}

export default App
