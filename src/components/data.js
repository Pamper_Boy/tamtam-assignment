import Florensis from '../assets/images/Slider/Florensis.jpg'
import Oxxio from '../assets/images/Slider/Oxxio.png'
import Walibi from '../assets/images/Slider/Walibi.jpg'

import instaItem_1 from '../assets/images/Feed/1.jpg'
import instaItem_2 from '../assets/images/Feed/2.jpg'
import instaItem_3 from '../assets/images/Feed/3.jpg'
import instaItem_4 from '../assets/images/Feed/4.jpg'
import instaItem_5 from '../assets/images/Feed/5.jpg'
import instaItem_6 from '../assets/images/Feed/6.jpg'


let slides = [
  {
    image: Florensis,
    title: "Florensis"
  },
  {
    image: Oxxio,
    title: "Oxxio"
  },
  {
    image: Walibi,
    title: "Walibi"
  },
]

let instagramItems = [
  {
    image: instaItem_1,
    text: "Last Thursday emerce organised their own ADE and we were part of it! Where are you gonna party tonight? #ADE#tbt #enight #tamtam"
  },
  {
    image: instaItem_2,
    text: "We're feeling happier and happier thanks to these nice dutch flowers we received from Florensis! #holland #flowers #doordebloemenofficenietmeerzien #settingthebloemetjesoutside"
  },
  {
    image: instaItem_3,
    text: "We are VERY VERY happy with our Silver Lovie Award and People's Lovies Winner for the Docters without borders website! #lovies #tamtamhome #proud #celebration"
  },
  {
    image: instaItem_4,
    text: "Thanks @radio538 and @maxpinas for bringing us friday beats with our brand new #marshall speakers! #friyay #gift #speaker #tamtamhome"
  },
  {
    image: instaItem_5,
    text: "Friyay! Coffee anyone? #almostweekend #tamtamhome #koffiemax #latteart"
  },
  {
    image: instaItem_6,
    text: "TamTam Talks Search Edition! First edition with our new colleagues from Expand Online👌💪🏻 #tamtamhome #tttalks #breakfast #search"
  },
]

export {
  slides,
  instagramItems
}
