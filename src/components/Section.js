import React, { PureComponent } from 'react'

class Section extends PureComponent {

  render() {

    const { title, text } = this.props

    return (
      <section className="section">
        <div className="section_container" >
          <h2 className="section_title" >{title}</h2>

          <p className="section_text" >{text}</p>
        </div>
      </section>
    )
  }
}

export default Section
