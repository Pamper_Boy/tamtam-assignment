import ImageSlider from './ImageSlider'
import Section from './Section'
import InstaFeed from './InstaFeed'

export {
  ImageSlider,
  Section,
  InstaFeed
}
