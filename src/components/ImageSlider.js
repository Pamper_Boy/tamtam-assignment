import React, { PureComponent, Fragment } from 'react'
import Button from './ui/Button'
import Scroll from './ui/Scroll'

import { slides } from './data.js'

class ImageSlider extends PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      currentSlide: 0,
      totalSlides: slides.length - 1,
      open: true,
      transitionDuration: 300
    };
  }

  setCurrentSlide(direction) {

    setTimeout(() => {
        this.setState({
          open: !this.state.open,
        })
        setTimeout(() => {
            this.setState({
              currentSlide: this.checkCurrentSlide(direction),
              open: !this.state.open,
            })
          }, this.state.transitionDuration);
      }, this.state.transitionDuration);
  }

  checkCurrentSlide = (direction) => {
    const { currentSlide, totalSlides } = this.state

    if(currentSlide === 0 && direction === "left" ) {
      return totalSlides
    } else if(currentSlide === totalSlides && direction === "right" ) {
      return 0
    } else if(currentSlide <= totalSlides && direction === "left" ) {
      return currentSlide - 1
    } else if(currentSlide <= totalSlides && direction === "right" ) {
      return currentSlide + 1
    }
  }

  render() {
    const { currentSlide } = this.state

    const classes = this.state.open ? '' : ' hide'
    let transitionStyle = {transitionDuration: `${this.state.transitionDuration}ms`}
    let imageStyle = { backgroundImage: `url(${slides[currentSlide].image})`,
                       transitionDuration: `${this.state.transitionDuration}ms`
                     }

    return (
      <Fragment>
        <div className="caroussel">
        <div className={"caroussel_image" + classes} style={ imageStyle }></div>

        <div className="caroussel_details">
          <h1 className={"caroussel_title" + classes} style={ transitionStyle }>
            {slides[currentSlide].title}
          </h1>
            <div className="caroussel_controls">

              <Button handleClick={() => this.setCurrentSlide("left")} icon="arrow-left" label=""/>
              <Button handleClick={null} icon="" label="View case"/>
              <Button handleClick={() => this.setCurrentSlide("right")} icon="arrow-right" label=""/>
            </div>
          </div>
          <Scroll/>
        </div>
      </Fragment>
    )
  }
}

export default ImageSlider
