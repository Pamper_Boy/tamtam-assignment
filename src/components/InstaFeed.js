import React, { PureComponent } from 'react'
import uuid4 from 'uuid4'

class Section extends PureComponent {

  render() {

    const { title, subtitle, items} = this.props

    return (
      <section className="instafeed">
        <div className="instafeed_container" >
          <h2 className="instafeed_title" >{title}</h2>
          <h3 className="instafeed_subtitle" >{subtitle}</h3>

          <div className="columns">
          {items.map(item => {
            return (
              <figure key={uuid4()}>
                <img src={item.image} alt=""/>
            	  <figcaption>{item.text}</figcaption>
            	</figure>
            )
          })}


        	</div>


        </div>
      </section>
    )
  }
}

export default Section
