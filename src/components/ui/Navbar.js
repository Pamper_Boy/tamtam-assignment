import React, { PureComponent } from 'react'
import { Link } from 'react-router-dom'

class Navbar extends PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      isOpen: false
    };
  }

  checkPath = (path) => {
    let currentPath = window.location.pathname
    return currentPath === path ? true : false
  }

  toggleMenu = () => {
    this.setState({
      isOpen: !this.state.isOpen
    })
  }

  render() {
    return (
      <header className="navbar">
        <Link to="/" onClick={this.toggleMenu} className={this.state.isOpen ? "menu_toggle menu_toggle--open" : "menu_toggle"} />
        <Link to="/" className="header_logo" />
        <nav className={ this.state.isOpen ? "menu-wrapper open" : "menu-wrapper close"}>
          <ul className="menu">
            <li className="menu_element"  data-active={this.checkPath("/")}>Home</li>
            <li className="menu_element"  data-active={this.checkPath("/people")}>People</li>
            <li className="menu_element"  data-active={this.checkPath("/contact")}>Contact</li>
          </ul>
        </nav>
      </header>
    )
  }
}

export default Navbar
