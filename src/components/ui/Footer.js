import React, { PureComponent } from 'react'
import { Link } from 'react-router-dom'


class Footer extends PureComponent {

  render() {
    return (
      <footer className="footer">
        <Link to="/" className="fb_logo" />
        <Link to="/" className="tw_logo" />
        <Link to="/" className="insta_logo" />

      </footer>
    )
  }
}

export default Footer
