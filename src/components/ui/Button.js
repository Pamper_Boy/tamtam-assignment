import React, { PureComponent } from 'react'
import { Link } from 'react-router-dom'


class Button extends PureComponent {

  render() {
    const { handleClick, icon, label } = this.props
    return (

      <Link to="/"
      onClick={ handleClick }
      className={!!icon ? "button button-icon" : "button"}>

        <i className={ icon }/>
        {!!label ? label : null}
      </Link>
    )
  }
}

export default Button
