import React, { PureComponent } from 'react'

class Scroll extends PureComponent {

  render() {
    return (
      <div className="scroll">
        <i className="arrow-down"/>
      </div>
    )
  }
}

export default Scroll
