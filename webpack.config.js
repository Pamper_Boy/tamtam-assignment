var webpack = require('webpack');
var path = require('path');

var BUILD_DIR = path.resolve(__dirname, 'public');
var APP_DIR = path.resolve(__dirname, 'src');

var config = {
  entry: APP_DIR + '/index.js',
  output: {
    path: BUILD_DIR,
    filename: 'bundle.js'
  },
  module: {
    rules: [{
        test: /\.css$/,
        use: [
          {
            loader: "style-loader" // creates style nodes from JS strings
          }, {
            loader: "css-loader" // translates CSS into CommonJS
          }, {
            loader: "sass-loader" // compiles Sass to CSS
          }
        ]},
        {
        test: /\.(png|jpg|gif|svg)$/,
        use: [
          {
            loader: 'file-loader',
            options: {}
          }
        ]},
      {
        test: /\.js?$/,
        include: APP_DIR,
        exclude: /node_modules/,
        loader: "babel-loader",
        query: {
          presets: ["env", "react"],
          plugins: ['transform-class-properties']
        }
      }]
  }
};

module.exports = config;


// var path = require('path');
// var webpack = require('webpack');
//
// module.exports = {
//   entry: './main.js',
//   output: { path: __dirname, filename: 'bundle.js' },
//   module: {
//     loaders: [
//       {
//         test: /.jsx?$/,
//         loader: 'babel-loader',
//         exclude: /node_modules/,
//         query: {
//           presets: ['es2015', 'react']
//         }
//       }
//     ]
//   },
// };

//
// var path = require("path")
//
// var DIST_DIR = path.resolve(__dirname, "dist")
// var SRC_DIR = path.resolve(__dirname, "src")
//
// var config = {
//   entry: SRC_DIR + "/index.js",
//   output: {
//     path: DIST_DIR + "/src",
//     filename: "bundle.js",
//     publicPath: "/src/"
//   },
//   module: {
//     loaders: [
//       {
//         test: /\.js?/,
//         include: SRC_DIR,
//         loader: "babel-loader",
//         quiry: {
//           presets: ["react", "env"]
//         }
//       }
//     ]
//   }
// }
//
// module.exports = config
